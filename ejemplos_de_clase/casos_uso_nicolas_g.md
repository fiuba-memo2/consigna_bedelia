Creación de aula

- Creo un aula exitosamente:

Ejemplo:
Cantidad de asientos: 10
Cantidad de ventanas: 3 
Tipo de bancos: pupitre_individual

- Creo un aula con tipo de banco inexistente:

Ejemplo:
Cantidad de asientos: 10
Cantidad de ventanas: 3 
Tipo de bancos: asd

Consulta de aulas

- Obtengo la información de todas las aulas exitosamente:

Ejemplo:
Me devuelve las aulas 1, 2 ,3 y 4 que son las que están creadas

- Consulto por un aula que no existe

Ejemplo:
Consulto por el aula 5 y esa aula no existe.

Pedido de aulas

- Pido un aula exitosamente

Ejemplo:
Pido un aula para el grupo siguiente
Cantidad de alumnos : 10
Requiere mesa_de_trabajo = Si
Dia: Lunes
Rango horario: de 19 a 22

Y se le asigna el aula 1
 
- Pido un aula para un grupo que requiere mesa de trabajo y no hay aulas disponibles

Ejemplo:
Pido un aula para el grupo siguiente
Cantidad de alumnos : 10
Requiere mesa_de_trabajo = Si
Dia: Lunes
Rango horario: de 19 a 22

No se le asigna ninguna aula porque no hay aulas con mesa de trabajo disponibles


- Pido un aula para un horario que están todas ocupadas

Ejemplo:
Dado la única aula existente:

Cantidad de asientos: 10
Cantidad de ventanas: 3 
Tipo de bancos: pupitre_individual

con el siguiente grupo asignado:

Cantidad de alumnos : 6
Requiere mesa_de_trabajo = no
Dia: Lunes
Rango horario: de 19 a 22

Entonces pedir un aula con los siguientes datos:

Cantidad de alumnos : 5
Requiere mesa_de_trabajo = no
Dia: Lunes
Rango horario: de 19 a 22

No deberia dejarme porque el horario esta ocupado

- Pido un aula que no alcanza la capacidad debido a falta de ventanas

Dado la única aula existente:

Cantidad de asientos: 10
Cantidad de ventanas: 0 
Tipo de bancos: pupitre_individual

Pido un aula con los siguientes datos:

Cantidad de alumnos : 10
Requiere mesa_de_trabajo = no
Dia: Lunes
Rango horario: de 19 a 22

Me dirá que no hay un aula disponible.
