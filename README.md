Bedelia
=======

Dominio del problema
---------------------

A partir de los protocolos de presencialidad el área de bedelia de la facultad necesita una aplicación para la asignación de aulas. 
Dada una configuración de aulas y un conjunto de pedidos de cursos, la aplicación debe asignar un aula a cada curso.

Las aulas se caracterizan por:
* Cantidad de asientos
* Cantidad de ventanas
* Tipo de bancos: pupitre_individual | pupitre_colectivo | mesa_de_trabajo

Tradicionalmente la capacidad de un aula está dada por la cantidad de asientos, pero según el protocolo vigente se aplican las siguientes restricciones de ventilación:

* si el aula no tiene ventanas su capacidad es 30% menos su cantidad de asientos
* Si el aula tiene 1 o 2 ventanas entonces su capacidad es un 10% menos que su cantidad de asientos
* Si el aula tiene más de 2 ventanas entonces su capacidad es igual a cantidad de asientos

Adicionalmente hay restricciones de distanciamiento para las aulas de tipo pupitre_colectivo las cuales tienen una capacidad restringida del 50 % (esta restricción tiene predencia por encima de las restricciones de ventilación).

Por otro lado, cada pedido de aula incluye el detalle de características requeridas:

* Cantidad de alumnos (cupo)
* ¿Requiere mesa_de_trabajo? 
* Dia: lunes | martes | miercoles | jueves | viernes | sabado


Funcionalidades a desarrollar
-----------------------------

A partir del contexto mencionado esto se debe construir una API HTTP/Rest que permita la siguientes operaciones.


### Creación de aula

Crea un aula en el sistema.

````
Request:
POST /aulas
{
  'nombre':'a1',
  'asientos': 4,
  'ventanas': 3,
  'tipo-bancos': 'pupitre_individual'
}

Response:
200
{ 
   'nombre':'a1',
   'asientos': 4,
   'ventanas': 3,
   'tipo-bancos': 'pupitre_individual',
   'capacidad': 4,
}
````

### Consulta de aulas

Devuelve la lista de aulas existentes.

````
Request GET /aulas

Response
200
[
    { 
        'nombre':'3',
        'asientos': 4,
        'ventanas': 1,
        'tipo-bancos': 'pupitre_colectivo'
    },
    { 
        'nombre':'4',
        'asientos': 14,
        'ventanas': 0,
        'tipo-bancos': 'pupitre_individual'
    }
]
````

### Pedido de aula

Dados los datos un curso, el sistema le asigna el primer aula disponible acorde a las necesidades del curso (primero llegado, primero asignado)

````
Request
POST /cursos
{
    'curso': 'memo2'
    'cupo': 10,
    'requiere_mesa_de_trabajo': false,
    'dia': 'lunes'
}

Response se asigna aula exitosamente
200
{
    'curso': 'memo2',
    'cupo': 10,
    'requiere_mesa_de_trabajo': false,
    'dia': 'lunes',
    'aula': '3'
}

Response cuando no se pudo asignar aula
201
{
    'curso': 'memo2',
    'cupo': 10,
    'requiere_mesa_de_trabajo': false,
    'dia': 'lunes',
    'aula': 'SIN_AULA_ASIGNADA'
}

````
### Consulta de curso

Dado un curso devuelve sus datos y el aula asignada
````
GET /cursos/:nombre_curso
200
{
    'curso': 'memo2',
    'cupo': 10,
    'requiere_mesa_de_trabajo': false,
    'dia': 'lunes',
    'aula': '3'
}
````

### Reasginación de aulas

Revisa todos los cursos y aulas existentes y vuelve a hacer la asignación de forma tal de minimizar la cantidad de aulas no asignadas.

````
Request
PUT /aulas/reasignar

Response
200
[
    {
        'curso': 'memo2',
        'cupo': 10,
        'requiere_mesa_de_trabajo': false,
        'dia': 'lunes',
        'aula': '3'
    },
    {
        'curso': 'algo2',
        'cupo': 60,
        'requiere_mesa_de_trabajo': false,
        'dia': 'lunes',
        'aula': '5'
    },
    {
        'curso': 'fisica 1',
        'cupo': 100,
        'requiere_mesa_de_trabajo': true,
        'dia': 'viernes',
        'aula': 'l5'
    }
]
````



### Reset
Esta funcionalidad vuelve el sistema a cero y es usadada solo a fines de prueba.
```
Request
POST /reset 

Response
200
{ "resultado": "ok"}
```
