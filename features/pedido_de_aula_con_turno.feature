#language: es
Característica: Pedido de aula

  Antecedentes:
    Dado que existe un aula "a1" con capacidad 200

  Escenario: pt01 - Se asigna el mismo aula en distinto turno
    Dado un curso "algo1" con 10 alumnos el dia "lunes" turno "tarde"
    Cuando pido un aula
    Entonces el curso es asignado al aula "a1"
    Dado un curso "algo2" con 10 alumnos el dia "lunes" turno "noche"
    Cuando pido un aula
    Entonces el curso es asignado al aula "a1"
    Dado un curso "algo3" con 10 alumnos el dia "lunes" turno "mañana"
    Cuando pido un aula
    Entonces el curso es asignado al aula "a1"

