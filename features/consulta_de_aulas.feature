#language: es
Característica: Consulta de aulas

  Antecedentes:
    Dado que existe un aula "a1" con capacidad 100
    Y que existe un aula "a2" con capacidad 120
    Y que existe un aula "a3" con capacidad 120

  Escenario: ca01 - Consulta de aulas
    Cuando consulto las aulas
    Entonces obtengo 3 aulas

  Escenario: ca02 - Se devuelven todas las aulas aunque haya asignaciones
    Dado un curso "algo1" con 100 alumnos el dia "lunes"
    Y pido un aula
    Cuando consulto las aulas
    Entonces obtengo 3 aulas
