Cuando('consulto el curso {string}') do |nombre_curso|
  get "/cursos/#{nombre_curso}"
end

Entonces('el curso no tiene aula') do
  expect(last_response.status).to eq 200
  aula_asignada = JSON.parse(last_response.body)['aula']
  expect(aula_asignada).to eq 'SIN_AULA_ASIGNADA'
end
