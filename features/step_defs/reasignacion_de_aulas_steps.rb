require_relative '../support/configuracion'

Dado('que existe un aula {string} con capacidad {int}') do |aula, capacidad|
  cuerpo_mensaje = {
    'nombre': aula,
    'asientos': capacidad,
    'ventanas': 3,
    'tipo-bancos': 'pupitre_individual'
  }
  post '/aulas', cuerpo_mensaje.to_json, 'CONTENT_TYPE' => 'application/json'
end

Dado('que existe un aula {string} con mesa de trabajo con capacidad {int}') do |aula, capacidad|
  cuerpo_mensaje = {
    'nombre': aula,
    'asientos': capacidad,
    'ventanas': 3,
    'tipo-bancos': 'mesa_de_trabajo'
  }
  post '/aulas', cuerpo_mensaje.to_json, 'CONTENT_TYPE' => 'application/json'
end

Dado('que existe un aula {string} con pupitre colectivo con capacidad {int}') do |aula, capacidad|
  cuerpo_mensaje = {
    'nombre': aula,
    'asientos': capacidad,
    'ventanas': 3,
    'tipo-bancos': 'pupitre_colectivo'
  }
  post '/aulas', cuerpo_mensaje.to_json, 'CONTENT_TYPE' => 'application/json'
end

Dado('que pido un aula para un curso {string} con {int} alumnos') do |curso, cupo|
  cuerpo_mensaje = {
    'curso': curso,
    'cupo': cupo,
    'requiere_mesa_de_trabajo': false,
    'dia': Config::DIA_LUNES,
    'turno': Config::TURNO_NOCHE
  }
  post '/cursos', cuerpo_mensaje.to_json, 'CONTENT_TYPE' => 'application/json'
end

Dado('que pido un aula para un curso {string} con {int} alumnos y que requiere mesa_de_trabajo') do |curso, cupo|
  cuerpo_mensaje = {
    'curso': curso,
    'cupo': cupo,
    'requiere_mesa_de_trabajo': true,
    'dia': Config::DIA_LUNES,
    'turno': Config::TURNO_NOCHE
  }
  post '/cursos', cuerpo_mensaje.to_json, 'CONTENT_TYPE' => 'application/json'
end

Entonces('el curso {string} está en el aula {string}') do |curso, aula_asignada|
  get "/cursos/#{curso}"
  expect(last_response.status).to eq 200
  aula = JSON.parse(last_response.body)['aula']
  expect(aula).to eq aula_asignada
end

Entonces('el curso {string} está sin aula') do |curso|
  get "/cursos/#{curso}"
  expect(last_response.status).to eq 200
  aula = JSON.parse(last_response.body)['aula']
  expect(aula).to eq 'SIN_AULA_ASIGNADA'
end

Cuando('pido reasignacion de aulas') do
  put '/aulas/reasignar', {}.to_json, 'CONTENT_TYPE' => 'application/json'
  expect(last_response.status).to eq 200
end
