Dado('un curso {string} con {int} alumnos el dia {string} turno {string}') do |curso, cupo, dia, turno|
  @curso = curso
  @cupo_alumnos = cupo
  @dia = dia
  @turno = turno
end
