#language: es
Característica: Reasignacion de aula

  Escenario: ra01 - Reasignacion simple
    Dado que existe un aula "a1" con capacidad 100
    Y que pido un aula para un curso "algo1" con 5 alumnos
    Entonces el curso "algo1" está en el aula "a1"
    Dado que existe un aula "a2" con capacidad 5
    Y que pido un aula para un curso "algo2" con 100 alumnos
    Entonces el curso "algo2" está sin aula
    Cuando pido reasignacion de aulas
    Entonces el curso "algo2" está en el aula "a1"
    Entonces el curso "algo1" está en el aula "a2"

  Escenario: ra02 - Reasignacion por mesa de trabajo
    Dado que existe un aula "aula-con-mesa-trabajo" con mesa de trabajo con capacidad 10
    Y que pido un aula para un curso "algo1" con 10 alumnos
    Entonces el curso "algo1" está en el aula "aula-con-mesa-trabajo"

    Dado que existe un aula "aula-comun" con capacidad 10
    Y que pido un aula para un curso "algo2-que-require-mesa" con 10 alumnos y que requiere mesa_de_trabajo
    Entonces el curso "algo2-que-require-mesa" está sin aula

    Cuando pido reasignacion de aulas

    Entonces el curso "algo1" está en el aula "aula-comun"
    Entonces el curso "algo2-que-require-mesa" está en el aula "aula-con-mesa-trabajo"

  Escenario: ra03 - Reasignacion por capacidad reducida
    Dado que existe un aula "a1" con pupitre colectivo con capacidad 30
    Y 4 ventanas
    Y que pido un aula para un curso "algo1" con 10 alumnos
    Entonces el curso "algo1" está en el aula "a1"

    Dado que existe un aula "a2" con pupitre colectivo con capacidad 20
    Y 2 ventanas
    Y que pido un aula para un curso "algo2" con 15 alumnos

    Entonces el curso "algo2" está sin aula
    Cuando pido reasignacion de aulas

    Entonces el curso "algo2" está en el aula "a1"
    Entonces el curso "algo1" está en el aula "a2"

  Escenario: ra04 - Reasignacion prioriza al que llego primero
    Dado que existe un aula "a1" con capacidad 30
    Y que pido un aula para un curso "algo1" con 10 alumnos
    Entonces el curso "algo1" está en el aula "a1"

    Y que pido un aula para un curso "algo2" con 30 alumnos

    Entonces el curso "algo2" está sin aula
    Cuando pido reasignacion de aulas

    Entonces el curso "algo2" está sin aula
    Entonces el curso "algo1" está en el aula "a1"
