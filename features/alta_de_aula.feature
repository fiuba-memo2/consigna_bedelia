#language: es
Característica: Alta de aula

  Escenario: aa01 - Alta exitosa de aula con muchas ventanas no disminuye capacidad
    Dada un aula "a1" con 10 asientos
    Y 5 ventanas
    Y bancos tipo "pupitre_individual"
    Cuando calculo su capacidad
    Entonces obtengo 10

  Escenario: aa02 - alta fallida por tipo de banco invalido
    Dada un aula "a1" con 10 asientos
    Y 0 ventanas
    Y bancos tipo "pupitre no-existe"
    Cuando calculo su capacidad
    Entonces obtengo un error

  Escenario: aa03 - Alta exitosa de aula sin ventanas disminuye capacidad 30 %
    Dada un aula "a1" con 10 asientos
    Y 0 ventanas
    Y bancos tipo "pupitre_individual"
    Cuando calculo su capacidad
    Entonces obtengo 7

  Escenario: aa04 - Alta exitosa de aula con 1 ventana disminuye capacidad 10 %
    Dada un aula "a1" con 10 asientos
    Y 1 ventanas
    Y bancos tipo "pupitre_individual"
    Cuando calculo su capacidad
    Entonces obtengo 9

  Escenario: aa05 - Alta exitosa de aula con pupitre_colectivo y muchas ventanas
    Dada un aula "a1" con 100 asientos
    Y 3 ventanas
    Y bancos tipo "pupitre_colectivo"
    Cuando calculo su capacidad
    Entonces obtengo 50

  Escenario: aa06 - Alta exitosa de aula con mesa_de_trabajo sin ventanas
    Dada un aula "a1" con 85 asientos
    Y 0 ventanas
    Y bancos tipo "mesa_de_trabajo"
    Cuando calculo su capacidad
    Entonces obtengo 59

  Escenario: aa07 - Alta exitosa de aula con pupitre_colectivo y 2 ventanas
    Dada un aula "a1" con 50 asientos
    Y 2 ventanas
    Y bancos tipo "pupitre_colectivo"
    Cuando calculo su capacidad
    Entonces obtengo 25

  Escenario: aa08 - Alta exitosa de aula con pupitre_colectivo y sin ventanas disminuye un 50%
    Dada un aula "a1" con 100 asientos
    Y 0 ventanas
    Y bancos tipo "pupitre_colectivo"
    Cuando calculo su capacidad
    Entonces obtengo 50

  Escenario: aa09 - Alta fallida de aula con ventanas negativas
    Dada un aula "a1" con 100 asientos
    Y -2 ventanas
    Y bancos tipo "pupitre_individual"
    Cuando calculo su capacidad
    Entonces obtengo un error

  Escenario: aa10 - Alta fallida de aula sin asientos
    Dada un aula "a1" con 0 asientos
    Y 2 ventanas
    Y bancos tipo "pupitre_individual"
    Cuando calculo su capacidad
    Entonces obtengo un error

  Escenario: aa11 - Alta exitosa de aula con pupitre_colectivo y 1 ventana - reduce su capacidad 50%
    Dada un aula "a1" con 100 asientos
    Y 1 ventanas
    Y bancos tipo "pupitre_colectivo"
    Cuando calculo su capacidad
    Entonces obtengo 50

  Escenario: aa12 - Alta fallida de aula con asientos negativos
    Dada un aula "a1" con -10 asientos
    Y 2 ventanas
    Y bancos tipo "pupitre_individual"
    Cuando calculo su capacidad
    Entonces obtengo un error

